from django.contrib import admin
from .models import *


@admin.register(Pilot)
class PilotAdmin(admin.ModelAdmin):
    pass


@admin.register(Port)
class PortAdmin(admin.ModelAdmin):
    pass


@admin.register(Spaceship)
class SpaceshipAdmin(admin.ModelAdmin):
    pass
