from django.db import models
from django.utils.translation import ugettext as _
from django.utils.translation import ugettext_lazy as _lazy
from people.models import Person
from locations.models import Planet, City


class Pilot(models.Model):

    person = models.OneToOneField(Person, on_delete=models.CASCADE, related_name='pilot')

    license_issuer = models.ForeignKey(Planet, on_delete=models.CASCADE, related_name='licensed_pilots')

    def __str__(self):
        return str(self.person)


class Port(models.Model):

    name = models.CharField(max_length=100)

    city = models.ForeignKey(City, on_delete=models.CASCADE, related_name='ports')

    def __str__(self):
        return self.name


class Spaceship(models.Model):

    name = models.CharField(max_length=100)

    model = models.CharField(max_length=100)

    pilot = models.ForeignKey(Pilot, on_delete=models.CASCADE, related_name='spaceships')

    port = models.ForeignKey(Port, on_delete=models.CASCADE, related_name='spaceships')

    def __str__(self):
        return self.name
