from django.conf.urls import url
from .views import *


app_name = 'spaceships'
urlpatterns = [
    url(r'^$', index, name='index'),
]
