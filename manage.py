#!/usr/bin/env python
import os
import sys
from importlib.util import find_spec


# Here the settings module is set for the *default* implementation of the
# project. This file is source-controlled, so don't change this value
# without first considering the preferred alternatives:
#
#   (1) Set the DJANGO_SETTINGS_MODULE environmental variable from the OS.
#       Environmental variables are very versatile; they should be capable
#       of handling the majority of use-cases. To temporarily set the 
#       variable on Linux, use:
#           export DJANGO_SETTINGS_MODULE=path.to.settings.module
#       To unset, use:
#           unset DJANGO_SETTINGS_MODULE
#       To temporarily set the variable on Windows, use:
#           set DJANGO_SETTINGS_MODULE=path.to.settings.module
#       To unset, use:
#           set DJANGO_SETTINGS_MODULE=
#
#   (2) Use the --settings command line argument to specify the exact
#       version of settings module desired.
#
DJANGO_SETTINGS_MODULE = "project.settings"

# In the event that no settings module path is provided via the OS
# environment or via the command line, then the DJANGO_SETTINGS_MODULE
# constant variable above is used. However, before importing the default, 
# this script will attempt to locate a machine-specific version of the 
# default settings. If present, this "local" settings module identified 
# by the LOCAL_SETTINGS_MODULE constant variable below will be imported
# instead. This settings module should exist purely on the local machine 
# and should not be source-controlled.
#
LOCAL_SETTINGS_MODULE = "project.versions.local.settings"

if __name__ == "__main__":

    if find_spec(LOCAL_SETTINGS_MODULE) is not None:
        DJANGO_SETTINGS_MODULE = LOCAL_SETTINGS_MODULE

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", DJANGO_SETTINGS_MODULE)

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
