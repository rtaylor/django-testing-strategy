Django Testing Strategy
=======================

A minimal example that demonstrates a sensible strategy for managing advanced testing and complex initial data in an arbitrary Django project.

Author
------

[Robert Taylor](mailto:rtaylor@pyrunner.com)


License
-------

MIT license
