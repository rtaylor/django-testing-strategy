from django.conf.urls import url
from .views import *


app_name = 'locations'
urlpatterns = [
    url(r'^$', index, name='index'),
]
