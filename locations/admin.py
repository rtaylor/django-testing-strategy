from django.contrib import admin
from .models import *


@admin.register(PlanetarySystemCategory)
class PlanetarySystemCategoryAdmin(admin.ModelAdmin):
    pass


@admin.register(PlanetarySystem)
class PlanetarySystemAdmin(admin.ModelAdmin):
    pass


@admin.register(PlanetCategory)
class PlanetCategoryAdmin(admin.ModelAdmin):
    pass


@admin.register(Planet)
class PlanetAdmin(admin.ModelAdmin):
    pass


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    pass
