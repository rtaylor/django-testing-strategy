from django.db import models
from django.utils.translation import ugettext as _
from django.utils.translation import ugettext_lazy as _lazy


class PlanetarySystemCategory(models.Model):

    code = models.CharField(max_length=5, unique=True)

    label = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = 'planetary system catagories'

    def __str__(self):
        return self.label


class PlanetarySystem(models.Model):

    name = models.CharField(max_length=100)

    category = models.ForeignKey(PlanetarySystemCategory, on_delete=models.CASCADE, related_name='planetary_systems')

    def __str__(self):
        return self.name


class PlanetCategory(models.Model):

    code = models.CharField(max_length=5, unique=True)

    label = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = 'planet catagories'

    def __str__(self):
        return self.label


class Planet(models.Model):

    name = models.CharField(max_length=100)

    category = models.ForeignKey(PlanetCategory, on_delete=models.CASCADE, related_name='planets')

    planetary_system = models.ForeignKey(PlanetarySystem, on_delete=models.CASCADE, related_name='planets')

    def __str__(self):
        return self.name


class City(models.Model):

    name = models.CharField(max_length=100)

    planet = models.ForeignKey(Planet, on_delete=models.CASCADE, related_name='cities')

    class Meta:
        verbose_name_plural = 'cities'

    def __str__(self):
        return self.name
