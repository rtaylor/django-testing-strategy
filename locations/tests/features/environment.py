import os
import django
from django.test.runner import DiscoverRunner
from django.test.testcases import LiveServerTestCase
from django.core.management import call_command


os.environ["DJANGO_SETTINGS_MODULE"] = "project.versions.test.settings"
django.setup()


def before_all(context):
    
    context.test_runner = DiscoverRunner()
    context.test_runner.setup_test_environment()
    context.old_db_config = context.test_runner.setup_databases()

    call_command('loaddata', 'initial', verbosity=0)


def before_scenario(context, scenario):
    context.test_case = LiveServerTestCase
    context.test_case.setUpClass()


def after_scenario(context, scenario):
    context.test_case.tearDownClass()
    del context.test_case


def after_all(context):
    context.test_runner.teardown_databases(context.old_db_config)
    context.test_runner.teardown_test_environment()
