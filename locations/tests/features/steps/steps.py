from behave import *
from locations.models import *


@when('we list all planetary system catagories')
def step_impl(context):
    context.category_list = [str(c) for c in PlanetarySystemCategory.objects.all()]

@then('the planetary system category {category} is in the list')
def step_impl(context, category):
    assert category in context.category_list
