Feature: testing the locations app
    the initial dataset is pre-loaded into the database
    
    Scenario Outline: initial planetary system catagories
        When we list all planetary system catagories
        then the planetary system category <pscategory> is in the list

    Examples: Planatery System Category
        | pscategory |
        | G2V        |
        | M4V        |

