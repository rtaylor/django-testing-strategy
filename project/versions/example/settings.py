from project.settings import *

# Point the project to a modified set of URLs
ROOT_URLCONF = 'project.versions.example.urls'

# Toggle DEBUG setting locally
DEBUG = True

# Add additional hosts specific to current machine
ALLOWED_HOSTS += ['localhost']

# Add or remove apps
INSTALLED_APPS = [app for app in INSTALLED_APPS if True]
