from django.conf.urls import url
from project.urls import *


urlpatterns = [u for u in urlpatterns if u.app_name not in ('admin', 'spaceships',)]
