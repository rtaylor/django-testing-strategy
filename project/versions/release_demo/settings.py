from project.settings import *

print('Importing ' + __name__)

ROOT_URLCONF = 'project.versions.release_demo.urls'

DEBUG = False

ALLOWED_HOSTS += ['localhost']

INSTALLED_APPS = [app for app in INSTALLED_APPS if app not in ('admin', 'spaceships',)]
