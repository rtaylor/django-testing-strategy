from unittest import TestCase
from unittest.mock import Mock, MagicMock, patch
from people import models, views


class PeopleTestCase(TestCase):

    def test_01(self):

        # test the description method
        p = models.Person(
            name='John Doe',
            age=17,
            sex=models.Person.MALE
            )
        self.assertEqual(p.description(), 'John Doe is a 17-year-old male')


    def test_02(self):

        from django.http import HttpResponse

        # test the index view
        request = Mock()
        result = views.index(request)
        self.assertIsInstance(result, HttpResponse)
        self.assertEqual(result.content, b'people index page')

        # test the my_people view
        request.user = Mock()
        with patch('people.views.Person') as mock_model:

            # user is not of User class (i.e., anonymous user)
            conf = {
                'objects.filter.side_effect': TypeError,
                'objects.filter.return_value': [
                    models.Person(name='John Doe'),
                    models.Person(name='Jane Doll'),
                    models.Person(name='Sally Dalton'),
                ],
                'objects.none.return_value': list(),
                }
            mock_model.configure_mock(**conf)
            result = views.my_people(request)
            self.assertIsInstance(result, HttpResponse)
            self.assertEqual(result.content, b'')

            # user manages a couple people
            mock_model.objects.filter.side_effect = None
            result = views.my_people(request)
            self.assertIsInstance(result, HttpResponse)
            self.assertEqual(result.content, b'John Doe | Jane Doll | Sally Dalton')
