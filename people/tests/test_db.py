from django.test import TestCase
from ..models import *


class PersonTestCase(TestCase):

    fixtures = ['initial']

    def test_01(self):

        # Person.age and Person.sex must meet basic criteria
        choices = [t[0] for t in Person.SEX_CHOICES]
        for p in Person.objects.all():
            self.assertTrue(p.age < 120 and p.age >= 0)
            self.assertTrue(p.sex in choices)

    def test_02(self):

        # these names must exist in initial data
        mandatory_names = [
            'John Bones',
            'Sally Hannis',
            'Francis McGonagle',
            'Carol Greathouse',
            'Theresa Gerhardt',
            'Dorothy Rake',
            ]
        ppl = Person.objects.filter(name__in=mandatory_names)
        self.assertEqual(len(ppl), len(mandatory_names))
        

    def test_03(self):
        
        # Dorothy must be managed by primary admin user
        p = Person.objects.get(name='Dorothy Rake')
        self.assertEqual(p.manager_id, 1)
        self.assertEqual(p.manager.username, 'myuser')


class RelationshipTestCase(TestCase):

    fixtures = ['initial']

    def test_01(self):

        # this relationship must exist in initial data
        r = Relationship.objects.get(
            person_1__name='Leslie Graves',
            category_1__code='DAU',
            person_2__name='Sally Hannis',
            category_2__code='MO'
            )

        # should not be able to add an exact duplicate relationship 
        from django.db.utils import IntegrityError
        with self.assertRaises(IntegrityError):
            r.pk = None
            r.save()
