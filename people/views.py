from django.shortcuts import render
from django.http import HttpResponse
from .models import Person


def index(request):
    return HttpResponse('people index page')


def my_people(request):

    try:
        ppl = Person.objects.filter(manager=request.user)
    except TypeError:
        ppl = Person.objects.none()

    return HttpResponse(' | '.join([p.name for p in ppl]))
    