from rest_framework import serializers
from .models import *


class PersonSerializer(serializers.ModelSerializer):

    class Meta:
        model = Person


class RelationshipCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = RelationshipCategory


class RelationshipSerializer(serializers.ModelSerializer):

    class Meta:
        model = Relationship
