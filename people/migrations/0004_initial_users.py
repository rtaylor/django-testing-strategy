# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-07-22 00:34
from __future__ import unicode_literals

from django.db import migrations
from django.contrib.auth.models import User


def insert_initial_users(apps, schema_editor):
    User.objects.create_superuser(
        id=1,
        username='myuser',
        password='password',
        email='myuser@example.com',
        first_name='John',
        last_name='Constantine'
        )


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0003_auto_20160721_2102'),
        ]

    operations = [
        migrations.RunPython(insert_initial_users),
        ]
