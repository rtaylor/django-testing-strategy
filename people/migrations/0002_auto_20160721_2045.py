# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-07-21 20:45
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Relationship',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.CreateModel(
            name='RelationshipCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=5, unique=True)),
                ('label', models.CharField(max_length=100)),
            ],
        ),
        migrations.AddField(
            model_name='relationship',
            name='category',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='relationships', to='people.RelationshipCategory'),
        ),
        migrations.AddField(
            model_name='relationship',
            name='person_1',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='relationships_1', to='people.Person'),
        ),
        migrations.AddField(
            model_name='relationship',
            name='person_2',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='relationships_2', to='people.Person'),
        ),
    ]
