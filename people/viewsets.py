from rest_framework import  viewsets
from .models import *
from .serializers import *


class PersonViewSet(viewsets.ModelViewSet):

    queryset = Person.objects.all()

    serializer_class = PersonSerializer


class RelationshipCategoryViewSet(viewsets.ModelViewSet):

    queryset = RelationshipCategory.objects.all()

    serializer_class = RelationshipCategorySerializer


class RelationshipViewSet(viewsets.ModelViewSet):

    queryset = Relationship.objects.all()

    serializer_class = RelationshipSerializer
