from django.db import models
from django.utils.translation import ugettext as _
from django.utils.translation import ugettext_lazy as _lazy
from django.contrib.auth.models import User


class Person(models.Model):

    MALE = 1

    FEMALE = 2

    SEX_CHOICES = (
        (MALE, _lazy('Male')),
        (FEMALE, _lazy('Female')),
        )

    name = models.CharField(max_length=100)

    age = models.PositiveSmallIntegerField()

    sex = models.PositiveSmallIntegerField(choices=SEX_CHOICES)

    manager = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name='managed_people')

    class Meta:
        verbose_name_plural = 'people'

    def __str__(self):
        return self.name

    def description(self):
        return '{} is a {}-year-old {}'.format(self.name, self.age, self.get_sex_display().lower())


class RelationshipCategory(models.Model):

    code = models.CharField(max_length=5, unique=True)

    label = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = 'relationship catagories'

    def __str__(self):
        return self.label


class Relationship(models.Model):

    person_1 = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='relationships_1')

    category_1 = models.ForeignKey(RelationshipCategory, on_delete=models.CASCADE, related_name='relationships_1')

    person_2 = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='relationships_2')

    category_2 = models.ForeignKey(RelationshipCategory, on_delete=models.CASCADE, related_name='relationships_2')

    class Meta:
        unique_together = ['person_1', 'category_1', 'person_2', 'category_2']

    def __str__(self):
        return '{} - {} ({}/{})'.format(self.person_1, self.person_2, self.category_1.code, self.category_2.code)
