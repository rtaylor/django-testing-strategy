from django.conf.urls import url, include
from rest_framework import routers
from .views import *
from .viewsets import *


router = routers.DefaultRouter()
router.register(r'person', PersonViewSet)
router.register(r'relationship_category', RelationshipCategoryViewSet)
router.register(r'relationship', RelationshipViewSet)


app_name = 'people'
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^my_people/$', my_people, name='my_people'),
    url(r'^api/', include(router.urls)),
]
